-- Insere massa de dados para Customer -- 
insert INTO customer (address, birth_date, city, cpf, email, name, password, phone, rg, salary, status, uf) VALUES ("Rua da Maria da Silva", "1985-05-02", "Ourinhos", "123456789", "maria@teste.com", "Maria da Silva", "123", "(14)123456789", "123456", 1500.00, "reprovado", "SP");
insert INTO customer (address, birth_date, city, cpf, email, name, password, phone, rg, salary, status, uf) VALUES ("Rua do Jo�o da Silva", "1978-08-06", "S�o Paulo", "321654987", "joao@teste.com", "Jo�o da Silva", "123", "(14)123456789", "321654", 1900.00, "aprovado", "SP");
insert INTO customer (address, birth_date, city, cpf, email, name, password, phone, rg, salary, status, uf) VALUES ("Rua do Gabriel da Silva", "1990-10-10", "S�o Paulo", "987654321", "gabriel@teste.com", "Gabriel da Silva", "123", "(14)123456789", "321654", 1500.00, "reprovado", "SP");
insert INTO customer (address, birth_date, city, cpf, email, name, password, phone, rg, salary, status, uf) VALUES ("Rua da Ana da Silva", "1980-11-06", "Santa Cruz", "087654321", "ana@teste.com", "Ana da Silva", "123", "(14)123456789", "321654", 2000.00, "pendente", "SP");
insert INTO customer (address, birth_date, city, cpf, email, name, password, phone, rg, salary, status, uf) VALUES ("Rua da Juliana da Silva", "1968-09-06", "Jacarezinho", "045623459", "juliana@teste.com", "Juliana da Silva", "123", "(14)123456789", "321654", 2500.00, "pendente", "PR");
insert INTO customer (address, birth_date, city, cpf, email, name, password, phone, rg, salary, status, uf) VALUES ("Rua do Gustavo da Silva", "1996-06-07", "Ourinhos", "687093258", "gustavo@teste.com", "Gustavo da Silva", "123", "(14)123456789", "321654", 3500.00, "pendente", "SP");

-- Insere massa de dados para UserType --
INSERT INTO user_type (description) VALUES ("analista de cr�dito");
INSERT INTO user_type (description) VALUES ("captador de propostas");

-- Insere massa de dados para User --
INSERT INTO USER (email, id_user_type, name, password, phone) VALUES ("jose@teste.com", 1, "Jos� da Silva", "123", "(14)90395848");
INSERT INTO USER (email, id_user_type, name, password, phone) VALUES ("camila@teste.com", 2, "Camila da Silva", "123", "(14)83495857");
INSERT INTO USER (email, id_user_type, name, password, phone) VALUES ("patricia@teste.com", 1, "Patricia da Silva", "123", "(14)21436578");
