# desafio-analise-credito

## Descrição
API de Sistema de análise de crédito para novos portadores de cartão desenvolvida com Spring Boot.

## Tecnologias

- [Java JDK 15](https://jdk.java.net/15/)
- [Maven](https://maven.apache.org/download.cgi)
- [Spring Boot](https://spring.io/projects/spring-boot)
- [MySQL](https://www.mysql.com/) 

## Como executar o projeto
Para instalar e executar o projeto insira estes comandos no terminal:

```
# Clone o repositório
$ git clone https://gitlab.com/yesminmarie/desafio-analise-credito/

# Entre na pasta do projeto
$ cd desafio-analise-credito/desafio-analise-credito

# Execute o projeto
$ ./mvnw clean install spring-boot:run
```

### DUMP

O DUMP com massa de dados pode ser baixado [aqui](https://gitlab.com/yesminmarie/desafio-analise-credito/-/blob/main/Dump.sql)

## Documentação
Após a execução e instalação do projeto, a documentação dos serviços REST poderá ser visualizada no seu navegador por meio da seguinte URL: http://localhost:8080/swagger-ui.html

## Testando a API
As requisições da API poderão ser testadas utilizando uma ferramenta de testes como o [Postman](https://www.postman.com/) ou o [Insomnia](https://insomnia.rest/). Outra opção é utilizar o próprio link da documentação http://localhost:8080/swagger-ui.html.

### Criação de usuários
Para criar um usuário, você pode utilizar a seguinte rota do tipo POST: 

http://localhost:8080/users


Veja um exemplo dos dados a serem enviados:

```
{
	"name": "José da Silva",
	"email": "teste@teste.com",
	"phone": "(14)12345678",
	"password": "123",
	 "id_user_type" : {
		 "id" : 1
	 }
}
```
Obs.: o "id" de "id_user_type" pode ser 1 (analista de crédito) ou 2 (captador de propostas)

### Listando usuários

Para listar todos os usuários utilize a seguinte rota do tipo GET:

http://localhost:8080/users/

### Criação de clientes

Para criar um cliente, você pode utilizar a seguinte rota do tipo POST: 

http://localhost:8080/customers

Exemplo dos dados a serem enviados:

```
{
  "name": "Maria da Silva",
  "address": "Rua da Maria da Silva",
  "birth_date": "1990-05-02",
  "city": "Ourinhos",
  "uf": "SP",
  "cpf": "1234567890",
  "email": "teste2@teste.com",
  "password": "123",
  "phone": "1234567",
  "rg": "847583940",
  "salary": 1500.00
}
```
Obs.: ao cadastrar o cliente, automaticamente seu status será registrado como "Pendente".

### Listando clientes

Para listar todos os clientes utilize a seguinte rota do tipo GET:

http://localhost:8080/customers/

### Pesquisando clientes por status

Para buscar clientes por um status específico, utilize a seguinte rota do tipo GET:

localhost:8080/customers/status/pendente

O exemplo acima listará todos os usuários com status "pendente". Você pode inserir como parâmetro de pesquisa os valores:

- pendente
- aprovado
- reprovado

### Consultando clientes por um campo específico

Você poderá escolher dados específicos para realizar uma pesquisa por um cliente. A rota GET a ser utilizada é a seguinte:

http://localhost:8080/customers/search

Exemplo do JSON:

```
{
    "name": "Maria da Silva"
}
```

### Pesquisando clientes por id

Para buscar um cliente por um id específico, utilize a rota do tipo GET passando o id como parâmetro:

http://localhost:8080/customers/3

### Alterando dados de um cliente

Para realizar alterações dos dados do cliente, utilize a seguinte rota do tipo PUT passando o número do id como parâmetro:

http://localhost:8080/customers/1

Exemplo do JSON:

```
{
  "name": "Maria da Silva",
  "cpf": "123456789",
  "rg": "123456",
  "email": "maria@teste.com",
  "phone": "(14)123456789",
  "address": "Rua da Maria da Silva",
  "city": "Ourinhos",
  "uf": "SP",
  "birth_date": "1985-05-02",
  "salary": 1500.0,
  "status": "aprovado",
  "password": "123"
}

```
### Removendo um cliente

Para remover um cliente, utilize a seguinte rota do tipo DELETE, passando id como parâmetro:

http://localhost:8080/customers/2
