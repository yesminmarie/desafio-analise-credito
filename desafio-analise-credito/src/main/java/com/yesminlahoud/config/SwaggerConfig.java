package com.yesminlahoud.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.yesminlahoud.controller")).paths(PathSelectors.any()).build()
				.apiInfo(getApInfo());
	}

	private ApiInfo getApInfo() {
		return new ApiInfo("Análise de Crédito API", "Esta é uma API para cadastro e acompanhamento de propostas de análise de crédito.", "1.0",
				"termos do serviço", new Contact("Yesmin Lahoud", "https://gitlab.com/yesminmarie/desafio-analise-credito", "ymslahoud@gmail.com"),
				"Licença Apache 2.0", "https://www.apache.org/licenses/LICENSE-2.0", Collections.emptyList());
	}
}
