package com.yesminlahoud.service;

import com.yesminlahoud.exception.ResourceNotFoundException;
import com.yesminlahoud.model.UserType;
import com.yesminlahoud.repository.UserTypeRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserTypeService {
    @Autowired
    private UserTypeRepository userTypeRepository;

    public UserType findById(Short id) {
        Optional<UserType> response = userTypeRepository.findById(id);
        
        if(response.isEmpty()) {
            throw new ResourceNotFoundException();
        }

        UserType userType = response.get();
        return userType;
    }

    public List<UserType> list() {
        List<UserType> response = userTypeRepository.findAll();
        return response;
    }

    public UserType create(UserType userType){
        UserType savedUserType = userTypeRepository.save(userType);
        return savedUserType;
    }

    public UserType delete(Short id){
        Optional<UserType> response = userTypeRepository.findById(id);

        if(response.isEmpty()) {
            throw new ResourceNotFoundException();
        }

        userTypeRepository.delete(response.get());
        UserType responseUserType = response.get();
        return responseUserType;
    }

    public UserType update (UserType userType){
        UserType updatedUserType = userTypeRepository.save(userType);
        return updatedUserType;
    }
}
