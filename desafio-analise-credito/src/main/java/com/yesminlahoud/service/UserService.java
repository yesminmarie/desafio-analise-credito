package com.yesminlahoud.service;

import java.util.List;
import java.util.Optional;

import com.yesminlahoud.exception.ResourceNotFoundException;
import com.yesminlahoud.model.User;
import com.yesminlahoud.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User findById(Long id) {
        Optional<User> response = userRepository.findById(id);
        
        if(response.isEmpty()) {
            throw new ResourceNotFoundException();
        }

        User user = response.get();
        return user;
    }

    public List<User> list() {
        List<User> response = userRepository.findAll();
        return response;
    }

    public User create(User user){
        User savedUser = userRepository.save(user);
        return savedUser;
    }

    public User delete(Long id){
        Optional<User> response = userRepository.findById(id);

        if(response.isEmpty()) {
            throw new ResourceNotFoundException();
        }

        userRepository.delete(response.get());
        User responseUser = response.get();
        return responseUser;
    }

    public User update (User user){
        User updatedUser = userRepository.save(user);
        return updatedUser;
    }
}
