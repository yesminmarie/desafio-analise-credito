package com.yesminlahoud.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.yesminlahoud.exception.ResourceNotFoundException;
import com.yesminlahoud.model.Customer;
import com.yesminlahoud.repository.CustomerRepository;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    public Customer findById(Long id) {
        Optional<Customer> response = customerRepository.findById(id);
        
        if(response.isEmpty()) {
            throw new ResourceNotFoundException();
        }

        Customer customer = response.get();
        return customer;
    }


    public List<Customer> list() {
        
        List<Customer> response = customerRepository.findAll();
        return response;
    }
    
    public List<Customer> listByExample(Customer customer) {
        
        List<Customer> response = customerRepository.findAll(Example.of(customer));
        return response;
    }

    public Customer create(Customer customer){
        customer.setStatus("Pendente");
        Customer savedCustomer = customerRepository.save(customer);
        return savedCustomer;
    }

    public Customer delete(Long id){
        Optional<Customer> response = customerRepository.findById(id);

        if(response.isEmpty()) {
            throw new ResourceNotFoundException();
        }

        customerRepository.delete(response.get());
        Customer responseCustomer = response.get();
        return responseCustomer;
    }

    public Customer update (Customer customer){
//    	Customer updatedCustomer = customerRepository.findById(customer.getId()).get();
//    	updatedCustomer.setStatus(customer.getStatus());
//    	return updatedCustomer;
    	
    	return customerRepository.save(customer);
    }
}
