package com.yesminlahoud.controller;

import java.util.List;
import com.yesminlahoud.service.UserService;
import com.yesminlahoud.exception.ResourceNotFoundException;
import com.yesminlahoud.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("users")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    @ApiOperation(value = "Lista usuários", notes = "Lista todos os usuários da aplicação.", response = User.class)
    public List<User> list() {
        List<User> user = userService.list();
        return user;
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Pesquisa usuário por id", notes = "Busca um usuário pelo id inserido como parâmetro na url.", response = User.class)
    public User findById(@PathVariable Long id) {
        try {
            User user = userService.findById(id);
            return user;
        } catch (ResourceNotFoundException error) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuário não encontrado", error);
        }
        
    }

    @PostMapping
    @ApiOperation(value = "Grava um usuário", notes = "Grava um novo usuário para a aplicação.", response = User.class)
    public ResponseEntity<User> create (@RequestBody User user) {
        User savedUser = userService.create(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedUser);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Deleta um usuário", notes = "Deleta um usuário da aplicação por meio do id inserido na url.", response = User.class)
    public User delete (@PathVariable Long id) {
        try {
            User deletedUser = userService.delete(id);
            return deletedUser;
        }catch (ResourceNotFoundException error) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuário não encontrado", error);
        }
        
    }

    @PutMapping
    @ApiOperation(value = "Atualiza um usuário", notes = "Atualiza os dados de um usuário.", response = User.class)
    public User update(@RequestBody User user) {
        User updatedUser = userService.update(user);
        return updatedUser;
    }
}
