package com.yesminlahoud.controller;

import com.yesminlahoud.service.UserTypeService;

import io.swagger.annotations.ApiOperation;

import com.yesminlahoud.exception.ResourceNotFoundException;
import com.yesminlahoud.model.User;
import com.yesminlahoud.model.UserType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("usertypes")
public class UserTypeController {
    @Autowired
    private UserTypeService userTypeService;

    @GetMapping
    @ApiOperation(value = "Lista os tipos de usuário", notes = "Lista os tipos de usuário da aplicação.", response = User.class)
    public List<UserType> list() {
        List<UserType> userType = userTypeService.list();
        return userType;
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Pesquisa tipo de usuário por id", notes = "Busca um tipo de usuário pelo id inserido como parâmetro na url.", response = User.class)
    public UserType findById(@PathVariable Short id) {
        try {
            UserType userType = userTypeService.findById(id);
            return userType;
        } catch (ResourceNotFoundException error) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Categoria não encontrada", error);
        }
        
    }

    @PostMapping
    @ApiOperation(value = "Grava um tipo de usuário", notes = "Grava um novo tipo de usuário para a aplicação.", response = User.class)
    public UserType create (@RequestBody UserType userType) {
        UserType savedUserType = userTypeService.create(userType);
        return savedUserType;
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Deleta um tipo de usuário", notes = "Deleta um tipo de usuário da aplicação por meio do id inserido na url.", response = User.class)
    public UserType delete (@PathVariable Short id) {
        try {
            UserType deletedUserType = userTypeService.delete(id);
            return deletedUserType;
        }catch (ResourceNotFoundException error) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Categoria não encontrada", error);
        }
        
    }

    @PutMapping
    @ApiOperation(value = "Atualiza um tipo de usuário", notes = "Atualiza a descrição de um tipo de usuário.", response = User.class)
    public UserType update(@RequestBody UserType userType) {
        UserType updatedUserType = userTypeService.update(userType);
        return updatedUserType;
    }
}
