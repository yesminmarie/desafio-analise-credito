package com.yesminlahoud.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.yesminlahoud.exception.ResourceNotFoundException;
import com.yesminlahoud.model.Customer;
import com.yesminlahoud.model.User;
import com.yesminlahoud.service.CustomerService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("customers")
public class CustomerController {

    private static String filesPath = "../uploads";

    @Autowired
    private CustomerService customerService;

    @GetMapping
    @ApiOperation(value = "Lista clientes", notes = "Lista todos os clientes da aplicação.", response = User.class)
    public List<Customer> list() {
        List<Customer> customer = customerService.list();
        return customer;
    }
    
    @GetMapping("/search")
    @ApiOperation(value = "Pesquisa clientes", notes = "Realiza uma pesquisa por meio de determinado dado do cliente inserido no corpo da requisição(nome, rg, cpf, etc.).", response = User.class)
    public List<Customer> listByExample(@RequestBody Customer customer) {
        List<Customer> customerList = customerService.listByExample(customer);
        return customerList;
    }
    
    @GetMapping("/status/{status}")
    @ApiOperation(value = "Pesquisa clientes por status (Pendente, Aprovado, Reprovado)", notes = "Realiza uma pesquisa de clientes pelo status inserido na url.", response = User.class)
    public List<Customer> listByStatus(@PathVariable String status) {
    	Customer findCustomer = new Customer();
    	findCustomer.setStatus(status);
        List<Customer> customerList = customerService.listByExample(findCustomer);
        return customerList;
    }

    // @GetMapping("/status")
    // public List<Customer> listByStatus() {
    //     List<Customer> customer = customerService.list();
    //     return customer;
    // }

    @GetMapping("/{id}")
    @ApiOperation(value = "Pesquisa cliente por id", notes = "Busca um cliente pelo id inserido como parâmetro na url.", response = User.class)
    public Customer findById(@PathVariable Long id) {
        try {
            Customer customer = customerService.findById(id);
            return customer;
        } catch (ResourceNotFoundException error) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente não encontrado", error);
        }
        
    }

    @PostMapping
    @ApiOperation(value = "Grava um cliente", notes = "Grava um novo cliente para a aplicação.", response = User.class)    
    public ResponseEntity<Customer> create (@RequestBody Customer customer) {
        Customer savedCustomer = customerService.create(customer);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedCustomer);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Deleta um cliente", notes = "Deleta um cliente da aplicação por meio do id inserido na url.", response = User.class)
    public Customer delete (@PathVariable Long id) {
        try {
            Customer deletedCustomer = customerService.delete(id);
            return deletedCustomer;
        }catch (ResourceNotFoundException error) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente não encontrado", error);
        }
        
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Atualiza um cliente", notes = "Atualiza os dados de um cliente.", response = User.class)
    public Customer update(@PathVariable Long id, @RequestBody Customer customer) {
    	customer.setId(id);
        Customer updatedCustomer = customerService.update(customer);
        return updatedCustomer;
    }
}
