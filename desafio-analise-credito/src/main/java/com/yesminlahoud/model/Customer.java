package com.yesminlahoud.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, nullable = false)
    private String name;

    @Column(length = 15, nullable = false)
    private String cpf;

    @Column(length = 15, nullable = false)
    private String rg;

    @Column(length = 50, nullable = false)
    private String email;

    @Column(length = 30, nullable = false)
    private String phone;
    
    @Column(length = 50, nullable = false)
    private String address;

    @Column(length = 50, nullable = false)
    private String city;

    @Column(length = 2, nullable = false)
    private String uf;

    @Column(nullable = false)
    private LocalDate birth_date;

    @Column
    private Double salary;

    // TODO - Implementar upload de arquivo de comprovante
    // @Column(nullable = false, unique = true)
    // private String income;

    @Column
    private String status;

    @Column(nullable = false)
    private String password;
}
