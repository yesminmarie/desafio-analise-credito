package com.yesminlahoud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioAnaliseCreditoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioAnaliseCreditoApplication.class, args);
	}

}
