package com.yesminlahoud.repository;

import com.yesminlahoud.model.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long>{
    
}
