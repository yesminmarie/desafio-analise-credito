package com.yesminlahoud.repository;

import com.yesminlahoud.model.UserType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserTypeRepository extends JpaRepository<UserType, Short> {
    
}
